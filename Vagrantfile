# -*- mode: ruby -*-
# vi: set ft=ruby :

# The most common configuration options are documented and commented below.
# For a complete reference, please see the online documentation at
# https://docs.vagrantup.com.
VAGRANTFILE_VERSION = "2"
Vagrant.require_version ">= 1.8.1"

# Function for checking if a set of plugins is installed
def require_plugins (names)
  list = names.is_a?(String) ? [names]: names
  list.reject! { |plugin| Vagrant.has_plugin?(plugin) }
  return if list.empty?
  raise "#{list.join ' '} is/are not installed. Please install with `vagrant plugin install #{list.join ' '}`."
end
require_plugins %w(vagrant-triggers)

# Function for reading YAML file with configuration details
require 'yaml'
def load_configuration (environment, filename)
  keyvaluemap = YAML.load_file(filename)[environment]
  raise "No environment #{environment} was found in #{filename}" unless keyvaluemap
  keyvaluemap["hostname"]||=keyvaluemap["project"]+"--"+environment
  keyvaluemap.merge( {"environment" => environment} )
end

Vagrant.configure(VAGRANTFILE_VERSION) do |config|

  # Load basic configuration of this VM
  configuration = load_configuration('localdev', 'provision/configuration.yml')
  vm_name = configuration["hostname"]
  vm_fqdn = vm_name + configuration["domain"]
  config.vm.define vm_name, primary: TRUE do |guest|

    # Place initial checks for VM host in this trigger
    guest.trigger.before :ALL do
      require_plugins %w(vagrant-bindfs vagrant-hostmanager vagrant-vbguest vagrant-cachier)
    end

    # Basic configuration for VirtualBox
    guest.vm.provider("virtualbox") { |vb| vb.name=vm_name; vb.cpus=2; vb.memory=2048 }
    guest.vm.box = "ubuntu/xenial64"
    guest.vm.network "private_network", type: "dhcp"

    # Define VM name and resolving its name to IP address
    guest.vm.hostname = vm_fqdn
    guest.hostmanager.enabled = true
    guest.hostmanager.manage_host = true
    guest.hostmanager.aliases = [vm_fqdn]
    guest.hostmanager.ip_resolver = proc do |vm, _|
      `VBoxManage guestproperty get #{vm.id} "/VirtualBox/GuestInfo/Net/1/V4/IP"`.split()[1] if vm.id
    end

    # Define shared folder between VM host and guest system
    guest.vm.synced_folder ".", "/vagrant-nfs", type: :nfs
    guest.bindfs.bind_folder "/vagrant-nfs", "/vagrant",
                               perms: "u=rwX:g=rwX:o=rX",
                               force_user: "www-data",
                               force_group: "www-data",
                               create_as_user: true

    # Initialize and trigger provision using Ansible
    guest.vm.synced_folder "./provision/", "/provision"
    guest.vm.provision :ansible_local, install: true
                               provisioning_path: "/provision/ansible",
                               galaxy_roles_path: "/provision/ansible/.requirements/roles",
                               galaxy_role_file: "requirements.yml",
                               playbook: "site.yml"
  end
end
